from flask import Flask, render_template, request
from database import users_collection, points_collection
user_collection = users_collection.collection
point_collection = points_collection.collection

app = Flask(__name__)
#paginas
@app.route("/")
def index():
    usuarios = lista_usuarios()
    pontos = lista_pontos()
    return render_template("index.html", user_len=len(usuarios), usuarios=usuarios, point_len=len(pontos), pontos=pontos)


@app.route("/AdicionarUsuario.html", methods=["GET", "POST"])
def adicionarUsuario():
    if (request.method == "GET"):
        return render_template("AdicionarUsuario.html")
    else:
        nome = request.form.get("nome")
        email = request.form.get("email")
        idade = request.form.get("idade")
        resposta = inserirUsuario(nome,email)
        return resposta

@app.route("/AdicionarPonto.html", methods=["GET", "POST"])
def adicionarPonto():
    if (request.method == "GET"):
        return render_template("AdicionarPonto.html")
    else:
        latitude = request.form.get("latitude")
        longitude = request.form.get("longitude")
        descricao = request.form.get("descricao")
        user_email = request.form.get("email")
        resposta = inserirPonto(latitude, longitude, descricao, user_email)
        return resposta


@app.route("/RemoverUsuario.html")
def removerUsuario():
    email = request.args.get('email')
    user_collection.delete_one({'email': email})
    return render_template("RemoverUsuario.html")

@app.route("/RemoverPonto.html")
def removerPonto():
    latitude = request.args.get('latitude')
    longitude = request.args.get('longitude')
    point_collection.delete_one({'latitude': latitude, 'longitude': longitude})
    return render_template("RemoverPonto.html")


@app.route("/AlterarUsuario.html", methods=["GET", "POST"])
def alterarUsuario():
    if (request.method == "GET"):
        email = request.args.get('email')
        nome = request.args.get('nome')
        return render_template("AlterarUsuario.html", email=email, nome=nome)
    else:
        antigo_nome = request.form.get("antigo_nome")
        novo_nome = request.form.get("nome")
        antigo_email = request.form.get("antigo_email")
        novo_email = request.form.get("email")
        user_collection.update_one({'nome': antigo_nome, 'email': antigo_email}, {'$set': {'nome':novo_nome,'email': novo_email}})
        return index()

@app.route("/AlterarPonto.html", methods=["GET", "POST"])
def alterarPonto():
    if (request.method == "GET"):
        latitude = request.args.get('latitude')
        longitude = request.args.get('longitude')
        descricao = request.args.get('descricao')
        email = request.args.get('email')
        return render_template("AlterarPonto.html", latitude=latitude, longitude=longitude, descricao=descricao, email=email)
    else:
        antiga_latitude = request.form.get("antiga_latitude")
        nova_latitude = request.form.get("latitude")
        antiga_longitude = request.form.get("antiga_longitude")
        nova_longitude = request.form.get("longitude")
        antiga_descricao = request.form.get("antiga_descricao")
        nova_descricao = request.form.get("descricao")
        antigo_email = request.form.get("antigo_email")
        novo_email = request.form.get("email")
        ##alterar_usuario(antiga_latitude,nova_latitude,antiga_longitude,nova_longitude,antiga_descricao,nova_descricao,antigo_email,novo_email)
        point_collection.update_one({'latitude': antiga_latitude, 'longitude': antiga_longitude}, {'$set': {'latitude': nova_latitude, 'longitude': nova_longitude, 'descricao': nova_descricao, 'user_email': novo_email}})
        return index()

#funções
def lista_usuarios():
    resultado = user_collection.find({})
    usuarios = []
    for dado in resultado:
        usuarios.append(dado)
    return usuarios

def lista_pontos():
    resultado = point_collection.find({})
    pontos = []
    for dado in resultado:
        pontos.append(dado)
    return pontos

def inserirPonto(latitude,longitude,descricao,user_email):
    dado = {
        'latitude': latitude,
        'longitude': longitude,
        'descricao': descricao,
        'user_email': user_email
    }
    if(user_collection.find_one({'email':user_email})):
        if (point_collection.find_one({'latitude': latitude}, {'longitude': longitude})):
            resposta = "<div class='avisos'><h1>Esse Ponto já foi cadastrado...</h1><br><a href=/><button id='btn_voltar'>Voltar</button></a>"
            return resposta
        else:
            point_collection.insert_one(dado)
            return index()
    else:
        resposta = "<div class='avisos'><h1>Email inexistente!</h1><br><a href=/><button id='btn_voltar'>Voltar</button></a>"
        return resposta


def inserirUsuario(nome,email):
    dado = {
        'nome': nome,
        'email': email
    }
    if(user_collection.find_one({'nome':nome,'email':email})):
        resposta = "<div class='avisos'><h1>Esse usuário já existe!</h1><br><a href=/><button id='btn_voltar'>Voltar</button></a>"
        return resposta
    else:
        user_collection.insert_one(dado)
        return index()
#run
if __name__ == "__main__":
    app.run(port=8080, host='0.0.0.0', debug=True, threaded=True)