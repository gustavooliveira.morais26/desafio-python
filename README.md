<h1>Sprint 4 - Desafio Python</h1>
<h2>Sobre</h2>
<ul>
<li>Projeto web feito com Python, Flask, MongoDB, HTML e CSS </li>
<h2>Primeiramente, execute os comandos abaixo:</h2>
<li> python -m pip install pymongo  </li>
<li> python -m pip install "pymongo[srv]"  </li>
<li> sudo apt get install python3-venv   </li>
<li> pip install Flask  </li>
<h2>Executando...</h2>
<li> Execute o arquivo main.py  </li>
<li> Abra seu navegador e acesse 127.0.0.1:8080  </li>
<h2>Referências utilizadas</h2>
<li>https://www.youtube.com/watch?v=YbyhVxffZfQ </li>
<li>https://www.youtube.com/watch?v=ME7vXWe9-v8 </li>
<li>https://www.youtube.com/watch?v=iu06CbqYx4k </li>
<li>https://www.youtube.com/watch?v=RYpV10216pw&list=PLefDXQ3nDsh-CZSay42ogB3-CcqI4WPYq&index=37 </li>
<li>https://www.youtube.com/watch?v=K2ejI4z8Mbg</li>
<li>https://www.youtube.com/watch?v=rGT5lG71pMQ</li>
</ul>
 <img src="https://mentorama.com.br/blog/wp-content/uploads/2022/06/capa-blog-coding-iniciante-1200x410.jpg">
