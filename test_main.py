import main
from database import users_collection, points_collection
def test_lista_usuarios():
    assert len(main.lista_usuarios()) == users_collection.db.users.count_documents({})

def test_lista_pontos():
    assert len(main.lista_pontos()) == points_collection.db.points.count_documents({})

def test_inserir_usuario():
    assert main.inserirUsuario("Gustavo de Oliveira Morais","gustavo@gmail.com") == "<div class='avisos'><h1>Esse usuário já existe!</h1><br><a href=/><button id='btn_voltar'>Voltar</button></a>"

